<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateContactTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("contact_messages", function($table) {
            $table->increments("id");
            $table->integer("site_id")->unsigned();
            $table->integer("locale_id")->unsigned();
            $table->integer("user_id")->unsigned()->nullable();
            $table->integer("ip")->unsigned();
            $table->string("country_code", 3)->nullable();
            $table->string("url", 1024);
            $table->integer("form_id");
            $table->string("name", 255);
            $table->string("email", 255);
            $table->string("subject", 255);
            $table->text("message");
            $table->tinyInteger("captcha");
            $table->string("recipients", 1024);
            $table->text("config");
            $table->timestamps();
            $table->engine = "InnoDB";
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("contact_messages");
    }

}
