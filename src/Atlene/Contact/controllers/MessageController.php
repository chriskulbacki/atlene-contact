<?php
namespace Atlene\Contact;

use View;
use Request;
use Atlene\Platform\CrudController;
use Atlene\Platform\Site;
use Atlene\Platform\Locale;
use Atlene\Platform\Asset;
use Atlene\Platform\Element;

class MessageController extends CrudController
{
    protected $modelName = "Atlene\Contact\Message";

    public function getFields()
    {
        $fields = [
            "id" => [
                "title" => tr("ID", "contact"),
                "list" => [
                    "display" => true,
                    "defaultSort" => true,
                    "defaultDesc" => true,
                ],
            ],
            "created_at" => [
                "title" => tr("Date", "contact"),
                "list" => true,
            ],
            "name" => [
                "title" => tr("Name", "contact"),
                "list" => true,
            ],
            "country_code" => [
                "title" => tr("Country", "contact"),
                "list" => true,
            ],
        ];

        if (Site::getCount() > 1) {
            $fields["site_id"] = [
                "title" => tr("Site", "contact"),
                "list" => true,
            ];
        }

        if (Locale::getCount() > 1) {
            $fields["locale_id"] = [
                "title" => tr("Locale", "contact"),
                "list" => true,
            ];
        }

        $fields["subject"] = [
            "title" => tr("Subject", "contact"),
            "list" => true,
        ];


        return $fields;
    }

    public function getListTopButtons($hasFilters = false)
    {
        // remove the top list buttons

        return false;
    }

    public function setListButtons($items)
    {
        // replace the edit/delete list buttons in the list with a view button

        foreach ($items as $item) {
            $item->buttons =
                Element::a(
                    false,
                    Element::i(false, ["class" => "fa fa-search"]),
                    [
                        "onclick" => "contact.showDetails('" . route("admin.contact.message", $item->id) . "')",
                        "class" => "btn btn-xs btn-default",
                        "title" => tr("Details", "contact"),
                    ]
                );
        }
    }

    public function processIndexItems($items)
    {
        // wrap the subject in a span (not used in the css for now)

        foreach ($items as $item) {
            $item->subject = Element::span($item->subject, ["class" => "contact-list-subject"]);
        }
    }

    /**
     * This function processes the ajax request for displaying the individual message details. It returns html with
     * the message information.
     *
     * @param integer $id
     * @return boolean
     */
    public function getMessage($id)
    {
        if (Request::ajax()) {
            $modelName = $this->modelName;

            if ($record = $modelName::find($id)) {
                return View::make(
                    "contact::message",
                    ["record" => $record, "config" => json_decode($record->config, true)]
                );
            }
        }

        return false;
    }

}
