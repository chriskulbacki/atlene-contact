<?php
namespace Atlene\Contact;

use Illuminate\Support\ServiceProvider;
use Route;
use Atlene\Platform\Menu;
use Atlene\Platform\SideMenuItem;
use Atlene\Platform\Asset;
use Atlene\Platform\Access;
use Atlene\Platform\Locale;
use Atlene\Platform\Backend;
use Atlene\Platform\Auth;

class ContactServiceProvider extends ServiceProvider {

    protected $defer = false;

    public function register()
    {
        // stop "composer update" from generating "class not found" errors
        if (!class_exists("\Atlene\Platform\Locale")) {
            return;
        }

        Locale::addGettextDomain("contact", realpath(__DIR__ . "/translations/"));
    }

    public function boot()
    {
        // stop "composer update" from generating "class not found" errors
        if (!class_exists("\Atlene\Platform\Locale")) {
            return;
        }

        // inform the system where the views are

        $this->loadViewsFrom(__DIR__ . "/views", "contact");

        // set config publishing path (use "php artisan vendor:publish --force" to publish)

        $this->publishes(
            [__DIR__ . "/config" => config_path()],
            "config"
        );

        // set asset publishing path

        $this->publishes(
            [__DIR__ . "/assets" => public_path("atlene/contact")],
            "public"
        );

        // include routes

        require __DIR__ . "/routes.php";

        // register contact the form admin permission
        Access::addRoute(
            tr("General", "contact"),
            "admin.contact*",
            tr("Access contact form messages", "contact"),
            tr("Allows access to the recorded messages sent using contact forms.", "contact")
        );

        // add the sidebar menu item
        Menu::findSideMenuItem("system")->add(
            SideMenuItem::make(tr("Contact messages", "contact"), "admin.contact.index")
        );

        Backend::addToHead("<link href='" . asset("atlene/contact/contact.css") . "' rel='stylesheet' />");
        Backend::addToHead("<script src='" . asset("atlene/contact/contact.js") . "'></script>");
    }

    public function provides()
    {
        return [];
    }

}
