
<div id='contact-form-container'>
    @if ($errorMessage)
        <div class='alert alert-danger'>{{ $errorMessage }}</div>
    @endif
    @if ($successMessage)
        <div class='alert alert-success'>{{ $successMessage }}</div>
    @endif
    {!! Form::open(["id" => "contact-form", "autocomplete" => "off"]) !!}

        {!! Form::hidden("form-id", $formId) !!}

        @if ($config['include_name'])
        <div class="form-group narrow{{ $errors->first('name') ? ' has-error' : '' }}">
            {!! Form::label("contact-name", tr("Your name", "contact"), ["class" => "control-label"]) !!}
            {!! Form::text("name", $name, ["id" => "contact-name", "class" => "form-control"]) !!}
            <div class='text-warning'>{{ $errors->first("name") }}</div>
        </div>
        @endif
        <div class="form-group narrow{{ $errors->first('email') ? ' has-error' : '' }}">
            {!! Form::label("contact-email", tr("Your email address", "contact"), ["class" => "control-label"]) !!}
            {!! Form::email("email", $email, ["id" => "contact-email", "class" => "form-control"]) !!}
            <div class='text-warning'>{{ $errors->first("email") }}</div>
        </div>
        @if ($config['include_subject'])
        <div class="form-group narrow{{ $errors->first('subject') ? ' has-error' : '' }}">
            {!! Form::label("contact-subject", tr("Subject", "contact"), ["class" => "control-label"]) !!}
            {!! Form::email("subject", $subject, ["id" => "contact-subject", "class" => "form-control"]) !!}
            <div class='text-warning'>{{ $errors->first("subject") }}</div>
        </div>
        @endif
        <div class="form-group{{ $errors->first('message') ? ' has-error' : '' }}">
            {!! Form::label("contact-message", tr("Message", "contact"), ["class" => "control-label"]) !!}
            {!! Form::textarea("message", $message, ["id" => "contact-message", "class" => "form-control"]) !!}
            <div class='text-warning'>{{ $errors->first("message") }}</div>
        </div>
        @if ($config['include_send_copy'])
        <div class="form-group">
            {!! Form::checkbox("copy", 1, $config['send_copy_checked'], ["id" => "contact-copy"]) !!}
            {!! Form::label("contact-copy", tr("Send yourself a copy", "contact"), ["class" => "control-label"]) !!}
        </div>
        @endif
        @if ($recaptcha)
        <div class="form-group recaptcha{{ $errors->first('g-recaptcha-response') ? ' has-error' : '' }}">
            {!! Form::label(false, tr("Spam protection", "contact"), ["class" => "control-label"]) !!}
            {!! $recaptcha !!}
            <div class='text-warning'>{{ $errors->first("g-recaptcha-response") }}</div>
        </div>
        @endif
        <div class="form-group button text-center">
            {!! Form::button(tr("Send message", "contact"), ["class" => "btn btn-primary btn-lg", "onclick" => "submitContactForm()"]) !!}
        </div>

    {!! Form::close() !!}

    <script>
    function submitContactForm()
    {
        var buttons = $("#contact-form .button .btn");
        buttons.attr("disabled", "disabled");

        $.ajax({
            url: '{{$url}}',
            type: "post",
            data: $("#contact-form").serialize(),
            success: function(result) {
                $("#contact-form-container").replaceWith(result);
            }
        });

        buttons.removeAttr("disabled");
    }
    </script>

    <style>
        #contact-form .form-group.narrow { width: 50%; }
    </style>
</div>
