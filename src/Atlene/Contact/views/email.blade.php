<p>{{ tr("Name:", "contact") }} {{ $input['name'] }}</p>
<p>{{ tr("Email:", "contact") }} {{ $input['email'] }}</p>
<p>{{ tr("Subject:", "contact") }} {{ $input['subject'] }}</p>
<p>{{ tr("Message:", "contact") }}</p>
<p>{{ $input['message'] }}</p>
