<h4 id='contact-message-subject'>
    {{ $record->subject}}
</h4>
<div id='contact-message-text'>
    {{ $record->message }}
</div>

<div id='contact-message-info'>
    <table class='table table-bordered table-condensed table-striped'>
        <tr>
            <td>{{ tr("Date", "contact") }}</td>
            <td>{{ $record->created_at}}</td>
        </tr>
        <tr>
            <td>{{ tr("Name", "contact") }}</td>
            <td>{{ $record->name }}</td>
        </tr>
        <tr>
            <td>{{ tr("Email", "contact") }}</td>
            <td>{{ $record->email }}</td>
        </tr>
        <tr>
            <td>{{ tr("IP", "contact") }}</td>
            <td>{{ long2ip($record->ip) }}</td>
        </tr>
        <tr>
            <td>{{ tr("Country", "contact") }}</td>
            <td>{{ $record->country_code}}</td>
        </tr>
        @if ($record->site_id)
        <tr>
            <td>{{ tr("Site", "contact") }}</td>
            <td>{{ $record->site_id}}</td>
        </tr>
        @endif
        @if ($record->locale_id)
        <tr>
            <td>{{ tr("Locale", "contact") }}</td>
            <td>{{ $record->locale_id}}</td>
        </tr>
        @endif
        <tr>
            <td>{{ tr("URL", "contact") }}</td>
            <td>{{ $record->url}}</td>
        </tr>
        <tr>
            <td>{{ tr("Form ID", "contact") }}</td>
            <td>{{ $record->form_id}}</td>
        </tr>
    </table>
</div>

@if (is_array($config))
    <div id='contact-message-config'>
        <table class='table table-bordered table-condensed table-striped'>
        @foreach ($config as $key => $val)
            <tr>
                <td>{{ $key }}</td>
                <td>{{ $val }}</td>
            </tr>
        @endforeach
        </table>
    </div>
@endif