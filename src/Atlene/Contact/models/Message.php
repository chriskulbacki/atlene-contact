<?php
namespace Atlene\Contact;

use Atlene\Platform\Crud;
use Atlene\Platform\Resource;
use Atlene\Platform\Site;
use Atlene\Platform\Locale;

class Message extends Crud
{
    protected $table = "contact_messages";

    /**
     * Convert country code to country name.
     *
     * @param string $value
     */
    public function getCountryCodeAttribute($value)
    {
        return Resource::getCountryNameByCode($value);
    }

    /**
     * Convert site id to site title.
     *
     * @param unknown $value
     * @return Ambigous <string, boolean>
     */
    public function getSiteIdAttribute($value)
    {
        return $value ? Site::get("title", $value) . " ($value)" : false;
    }

    /**
     * Convert locale id to locale name.
     *
     * @param unknown $value
     * @return Ambigous <string, boolean>
     */
    public function getLocaleIdAttribute($value)
    {
        return $value ? Locale::get("name", $value) . " ($value)" : false;
    }
}
