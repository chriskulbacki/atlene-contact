<?php
namespace Atlene\Contact;

use View;
use URL;
use Controller;
use Validator;
use Input;
use Request;
use Config;
use Mail;
use Session;
use Auth;
use Atlene\Platform\Strings;
use Atlene\Platform\Element;
use Atlene\Platform\Locale;
use Atlene\Platform\Site;

class Contact extends \App\Http\Controllers\Controller
{
    /**
     * Returns the html of the contact form. This function should be used to obtain the contact form and include
     * it in the page view.
     *
     * The default contact form settings can be overriden by passing new values as the function array parameter.
     *
     * @param array $configOverride
     */
    public static function getForm(array $configOverride = [])
    {
        $config = array_merge(Config::get("atlene.contact"), $configOverride);

        if (empty($config['recipients'])) {
            $config['recipients'] = Config::get("mail.from")['address'];
        }

        if (empty($config['recaptcha_site_key']) || empty($config['recaptcha_secret_key'])) {
            $config['include_recaptcha'] = false;
        }

        $config['recipients'] = explode(",", $config['recipients']);

        return self::getFormHtml($config);
    }

    /**
     * Gets the html of the contact form. This function generates the id of the form based on its config values,
     * includes it as a hidden input on the contact form and saves the id and the corresponding config in the session.
     * When the form post input is processed, the correct config is retrieved from the session and used. This way
     * it's possible to include a number of contact forms on different pages of the website, each one using
     * different config settings.
     *
     * @param array $config
     * @param string $successMessage
     * @param string $validator
     * @param string $errorMessage
     * @return string
     */
    private static function getFormHtml(
        array $config,
        $successMessage = false,
        $validator = false,
        $errorMessage = false
    ) {
        // create the recaptcha html code

        if ($config['include_recaptcha']) {
            $script = Element::script(
                "",
                [
                    "src" => "//www.google.com/recaptcha/api.js?hl=" .
                        ($config['recaptcha_language'] ? $config['recaptcha_language'] : Locale::get("code"))
                ]
            );

            $div = Element::div("", ["class" => "g-recaptcha", "data-sitekey" => $config['recaptcha_site_key']]);

            if ($config['recaptcha_theme']) {
                $div->setAttr("data-theme", $config['recaptcha_theme']);
            }

            $recaptcha = $script . "\n" . $div;
        } else {
            $recaptcha = false;
        }

        // get the form id and save the config to the session, it'll be used on post

        $formId = self::getFormId($config);
        Session::put("contact-form-$formId", $config);

        $view = View::make(
            override("contact::contact"),
            [
                "formId" => $formId,
                "url" => URL::route(override("contact.form.post")),
                "recaptcha" => $recaptcha,
                "config" => $config,
                "name" => $validator ? Input::get("name") : false,
                "email" => $validator ? Input::get("email") : false,
                "subject" => $validator ? Input::get("subject") : false,
                "message" => $validator ? Input::get("message") : false,
                "errorMessage" => $errorMessage,
                "successMessage" => $successMessage,
            ]
        );

        if ($validator) {
            $view = $view->withErrors($validator);
        }

        return $view->render();
    }

    /**
     * Processes the post from the contact form. Returns the html of the contact form with errors or success. The
     * returned html replaces the contact form code using javascript.
     *
     * @return string
     */
    public static function postForm()
    {
        $config = Session::get("contact-form-" . Input::get("form-id"), Config::get("contact::config"));
        $strings = Strings::validation();

        $rules = [
            "email" => "required|email",
            "message" => "required",
        ];

        $config['include_name'] && ($rules['name'] = "required");
        $config['include_subject'] && ($rules['subject'] = "required");

        // validate recaptcha
        if ($config['include_recaptcha']) {
            // if captcha box is not checked, the input is not present and validation doesn't engage, let's set it
            if (!Input::has("g-recaptcha-response")) {
                Input::merge(["g-recaptcha-response" => "invalid"]);
            }

            $rules["g-recaptcha-response"] = "recaptcha";
            $strings['recaptcha'] = tr("Please check the verification box.", "contact");
            self::extendValidatorWithCaptcha($config);
        }

        $validator = Validator::make(Input::all(), $rules, $strings);

        try {
            if ($validator->fails()) {
                throw new \Exception(tr("Please correct the errors on the form.", "contact"));
            }

            if (!self::save($config)) {
                throw new \Exception(tr("Cannot save the message.", "contact"));
            }

            Mail::send(
                override("contact::email"),
                ["input" => Input::all()],
                function($message) use ($config) {
                    $message->to($config['recipients'])
                        ->subject(sprintf(tr("Email from contact form at %s", "contact"), Request::getHost()));

                    if ($config['include_send_copy'] && Input::has("copy")) {
                        $message->cc(Input::get("email"));
                    }
                }
            );

        } catch (\Exception $e) {
            return self::getFormHtml(
                $config,
                false,
                $validator,
                $e->getMessage(),
                true
            );
        }

        return self::getFormHtml($config, tr("Your message has been sent.", "contact"));
    }

    /**
     * Returns the id of the form obtained by crc'ing the form's config values.
     *
     * @param array $config
     * @return integer
     */
    private static function getFormId(array $config)
    {
        return crc32(json_encode($config));
    }

    /**
     * Extends the validator with a captcha input.
     *
     * @param array $config
     * @return null
     */
    private static function extendValidatorWithCaptcha(array $config)
    {
        Validator::extend('recaptcha', function($attribute, $value, $parameters) use ($config) {
            $response = file_get_contents(
                "https://www.google.com/recaptcha/api/siteverify" .
                "?secret=" . $config['recaptcha_secret_key'] .
                "&response=" . Input::get("g-recaptcha-response") .
                "&remoteip={$_SERVER["REMOTE_ADDR"]}"
            );

            $response = json_decode($response, true);
            return is_array($response) && $response['success'];
        });
    }

    /**
     * Saves the submitted contact information in the database.
     *
     * @param array $config
     */
    private static function save(array $config)
    {
        $recipients = implode(",", $config['recipients']);
        unset($config['recipients']);

        $ip = isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : 0;
        $country = null;

        // get country from ip
        if ($ip) {
            $reply = @file_get_contents("http://ip2c.org/$ip");
            if ($reply && $reply[0] == "1") {
                $reply = explode(";", $reply);
                $country = $reply[1];
            }
        }

        $message = new Message;

        $message->site_id = Site::getId();
        $message->locale_id = Locale::getId();
        $message->user_id = Auth::id();
        $message->ip = ip2long($ip);
        $message->country_code = $country;
        $message->url = Request::url();
        $message->form_id = Input::get("form-id");
        $message->name = Input::get("name");
        $message->email= Input::get("email");
        $message->subject = Input::get("subject");
        $message->message = Input::get("message");
        $message->captcha = $config['include_recaptcha'];
        $message->recipients = $recipients;
        $message->config = json_encode($config);

        return $message->save();
    }

}
