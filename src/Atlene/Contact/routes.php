<?php

// route that will process the posts from the contact forms
Route::post(
    "/contact-form-post",
    ["as" => "contact.form.post", "uses" => "Atlene\Contact\Contact@postForm"]
);

Route::group(
    ["prefix" => override("admin")],
    function () {

        // admin url to display a list of recorded messages (crud-compatible)
        Route::get(
            override("contact"),
            [
                "as" => "admin.contact.index",
                "uses" => override("Atlene\Contact\MessageController") . "@index",
            ]
        );

        // admin url to obtain message details via ajax
        Route::post(
            "/contact/{id}",
            [
                "as" => "admin.contact.message",
                "uses" => override("Atlene\Contact\MessageController") . "@getMessage"
            ]
        );
    }
);