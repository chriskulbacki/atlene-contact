<?php

/*
 * These config settings can be set individually for each form by overriding them in the parameter of the
 * Contact::getForm($configOverride) function.
 */

return [
    // fields to include

    "include_name" => true,
    "include_subject" => true,
    "include_send_copy" => true,
    "include_recaptcha" => false,

    // recipients, a comma separated list, if false, it will default to Config::get("mail.from")['address']

    "recipients" => false,

    // send copy settings

    "send_copy_checked" => false,

    // recaptcha settings

    "recaptcha_site_key" => "",
    "recaptcha_secret_key" => "",
    "recaptcha_theme" => false,
    "recaptcha_language" => false,
];