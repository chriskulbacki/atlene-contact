
var contact = new function()
{
    this.showDetails = function(url)
    {
        dialog.make("contact-message-box");
        $("#contact-message-box").addClass("full-screen");
        dialog.show("contact-message-box");
        
        $.ajax({
            url: url,
            type: "POST",
            data: {_token: csrfToken},
            success: function(result) {
                $("#contact-message-box .dialog-content").html(result);
            }
        });
    }
   
}  