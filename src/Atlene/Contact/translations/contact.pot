# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: PACKAGE VERSION\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-01-30 14:48+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=CHARSET\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../Atlene/Contact/Contact.php:142
msgid "Please check the verification box."
msgstr ""

#: ../Atlene/Contact/Contact.php:150
msgid "Please correct the errors on the form."
msgstr ""

#: ../Atlene/Contact/Contact.php:154
msgid "Cannot save the message."
msgstr ""

#: ../Atlene/Contact/Contact.php:162
#, php-format
msgid "Email from contact form at %s"
msgstr ""

#: ../Atlene/Contact/Contact.php:180
msgid "Your message has been sent."
msgstr ""

#: ../Atlene/Contact/ContactServiceProvider.php:63
msgid "General"
msgstr ""

#: ../Atlene/Contact/ContactServiceProvider.php:65
msgid "Access contact form messages"
msgstr ""

#: ../Atlene/Contact/ContactServiceProvider.php:66
msgid "Allows access to the recorded messages sent using contact forms."
msgstr ""

#: ../Atlene/Contact/ContactServiceProvider.php:71
msgid "Contact messages"
msgstr ""

#: ../Atlene/Contact/MessageController.php:20
msgid "ID"
msgstr ""

#: ../Atlene/Contact/MessageController.php:28 /tmp/_views_message.blade.php:11
msgid "Date"
msgstr ""

#: ../Atlene/Contact/MessageController.php:32 /tmp/_views_message.blade.php:15
msgid "Name"
msgstr ""

#: ../Atlene/Contact/MessageController.php:36 /tmp/_views_message.blade.php:27
msgid "Country"
msgstr ""

#: ../Atlene/Contact/MessageController.php:43 /tmp/_views_message.blade.php:32
msgid "Site"
msgstr ""

#: ../Atlene/Contact/MessageController.php:50 /tmp/_views_message.blade.php:38
msgid "Locale"
msgstr ""

#: ../Atlene/Contact/MessageController.php:56 /tmp/_views_contact.blade.php:27
msgid "Subject"
msgstr ""

#: ../Atlene/Contact/MessageController.php:83
msgid "Details"
msgstr ""

#: /tmp/_views_contact.blade.php:15
msgid "Your name"
msgstr ""

#: /tmp/_views_contact.blade.php:21
msgid "Your email address"
msgstr ""

#: /tmp/_views_contact.blade.php:33
msgid "Message"
msgstr ""

#: /tmp/_views_contact.blade.php:40
msgid "Send yourself a copy"
msgstr ""

#: /tmp/_views_contact.blade.php:45
msgid "Spam protection"
msgstr ""

#: /tmp/_views_contact.blade.php:51
msgid "Send message"
msgstr ""

#: /tmp/_views_email.blade.php:1
msgid "Name:"
msgstr ""

#: /tmp/_views_email.blade.php:2
msgid "Email:"
msgstr ""

#: /tmp/_views_email.blade.php:3
msgid "Subject:"
msgstr ""

#: /tmp/_views_email.blade.php:4
msgid "Message:"
msgstr ""

#: /tmp/_views_message.blade.php:19
msgid "Email"
msgstr ""

#: /tmp/_views_message.blade.php:23
msgid "IP"
msgstr ""

#: /tmp/_views_message.blade.php:43
msgid "URL"
msgstr ""

#: /tmp/_views_message.blade.php:47
msgid "Form ID"
msgstr ""
