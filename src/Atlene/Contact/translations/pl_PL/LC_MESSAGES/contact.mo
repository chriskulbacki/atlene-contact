��             +         �     �  @   �     '     @     Q     Y     ^     f     l     �     �     �     �     �     �     �     �     �     �  "   �  &   �          !     6     ;     K     S     \     `     s  	   �  �  �  ,   :  7   g      �     �     �     �     �     �     �               +     3     6     9     @     L     Y     _  %   f  (   �     �     �     �     �                      $   .     S                
             	                                                                                                                   Access contact form messages Allows access to the recorded messages sent using contact forms. Cannot save the message. Contact messages Country Date Details Email Email from contact form at %s Email: Form ID General ID IP Locale Message Message: Name Name: Please check the verification box. Please correct the errors on the form. Send message Send yourself a copy Site Spam protection Subject Subject: URL Your email address Your message has been sent. Your name Project-Id-Version: 
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-01-30 14:48+0100
PO-Revision-Date: 2015-01-30 19:58+0100
Last-Translator: Chris <chris@sprigo.com>
Language-Team: 
Language: pl_PL
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Generator: Poedit 1.5.4
Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2);
 Dostęp do wiadomości z formularzy kontaktu Pozwala na dostęp do wiadomości z formularzy kontaktu Nie można zapisać wiadomości. Wiadomości formularzy kontaktu Kraj Data Detale Email Wiadomość z formularza na %s Email: ID formularza Ogólne ID IP Język Wiadomość Wiadomość: Imię Imię: Proszę zaznaczyć pole weryfikujące Proszę poprawić błędy na formularzu. Wyślij wiadomość Wyślij kopię na swój adres Witryna Zabezpieczenie przed spamem Temat Temat: URL Twój adres email Twoja wiadomość została wysłana. Twoje imię 